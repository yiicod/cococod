<?php

namespace yiicod\cococod\actions;

/*
 * CocoCodAction
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * @version $Id$
 * @package extensions
 * @since 1.0
 */

use CAction;
use Yii;

class FileRenderAction extends CAction
{
    /**
     * Callable.
     *
     * @var type
     */
    public $model = [];

    /**
     * Filed name.
     *
     * @var string
     */
    public $field = null;

    /**
     * this action invokes the appropiated handler referenced by a 'classname' url attribute.
     * the specified classname must implements: EYuiActionRunnable.php.
     */
    public function run()
    {
        header('Content-Description: File Transfer');
        header('Status: 200 Ok');
        header('Content-Type: '.$this->getFileType($this->field));
        header('Content-Disposition: attachment; filename='.basename($this->getOwner()->{$this->field}));
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $model = call_user_func($this->model);
        readfile($model->getFilePath($this->field));
        Yii::app()->end();
    }
}
