<?php

namespace yiicod\cococod\models\base;

interface UploaderInterface
{
    public function uploading($fullFileName, $userdata, $results);
}
