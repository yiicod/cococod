<?php

namespace yiicod\cococod\models\behaviors;

use CActiveRecordBehavior;
use CEvent;
use CException;
use CFileHelper;
use CWebApplication;
use Yii;
use yiicod\cococod\helpers\FileUploadHelper;

/**
 * Coco behavior uploader
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class FileUploadBehavior extends CActiveRecordBehavior
{
    /**
     * The public url for file
     * 'url' => Yii::app()->getBaseUrl(true) . '/uploads'
     */
    public $uploadUrl;

    /**
     * The directory path for file
     * 'uploadDir' => Yii::getPathOfAlias('webroot') . '/uploads'
     */
    public $uploadDir;

    /**
     * @param int $primaryKey Primary key
     */
    public $primaryKey = 'id';

    /**
     * An array where keys are fields that contain file
     */
    public $fields = array();

    /**
     * mkdir mode
     * @var int
     */
    public $mode = 0755;

    /**
     * Prepare status
     */
    protected $fieldsValues = array();

    /**
     * Attributes what was found before update
     */
    protected $attributes = array();

    /**
     * Get file by session key
     * @param string $field
     * @param bool Retrun only file name
     * @return mixed Return string or null
     */
    public function getFileByKey($field, $onlyName = false)
    {
        if (Yii::app() instanceof CWebApplication) {
            // @todo : have fixed to up stage, maybe wrong, need to look and fix
            $file = Yii::app()->session->get($this->generateFileKey($field));
            if ($onlyName) {
                $explodeFilePath = explode('/', $file);
                $file = end($explodeFilePath);
            }
            return $file;
        }
        return null;
    }

    /**
     * Remove file by session key
     * @param string $field
     * @param bool $attrClear
     * @return void
     */
    public function resetFile($field, $attrClear = false)
    {
        if (Yii::app() instanceof CWebApplication) {
            @unlink(Yii::app()->session->get($this->generateFileKey($field)));
            if ($attrClear === true && $this->owner->hasAttribute($field)) {
                $this->owner->{$field} = null;
            }
            Yii::app()->session->add($this->generateFileKey($field), '');
        }
    }

    /**
     * Remove field value and relative physical file
     * @param string $field
     */
    public function removeFile($field)
    {
        if (false === is_null(Yii::app()->getComponent('emitter'))) {
            Yii::app()->emitter->emit('yiicod.cococod.models.behaviors.' . $this->owner->tableName() . '.' . $field . '.rrmdir', array(
                new CEvent($this, array(
                    'uploadDir' => rtrim($this->uploadDir, '/'),
                    'folderPath' => $this->getFilePath($field)
                ))
            ));
        }
        $this->owner->{$field} = null;
        $this->owner->save(false);

        FileUploadHelper::rrmdir($this->getFilePath($field));
    }

    /**
     * Remove all files relative to model
     */
    public function removeFiles()
    {
        if (false === is_null(Yii::app()->getComponent('emitter'))) {
            Yii::app()->emitter->emit('yiicod.cococod.models.behaviors.' . $this->owner->tableName() . '.rrmdir', array(
                new CEvent($this, array(
                    'uploadDir' => rtrim($this->uploadDir, '/'),
                    'folderPath' => $this->getFolderPath()
                ))
            ));
        }

        FileUploadHelper::rrmdir($this->getFolderPath());
    }

    /**
     * Save file to ssesion, after move to "getFilePath"
     * @author Orlov Alexey <aaorlov88@gmail.com>
     */
    public function onAfterFileUploaded($fullFileName, $field)
    {
        Yii::app()->session->add($this->generateFileKey($field), $fullFileName);
    }

    /**
     * Get relative folder path without uploadDir
     * @return string
     */
    public function getRelativeFolderPath()
    {
        return '/' . lcfirst($this->getOwner()->tableName()) . '/' . $this->getOwner()->{$this->primaryKey};
    }

    /**
     * Get folder path
     * @param string $field Field name
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @return string Return path to entity img with|out field name
     */
    public function getFolderPath($field = null)
    {
        $path = rtrim($this->uploadDir, '/') . $this->getRelativeFolderPath();

        if (is_null($field)) {
            return $path;
        }

        return (rtrim($path, '/') . '/' . $field . '/');
    }

    /**
     * Get file path by "getFolderPath"
     * @param string $field Field name
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @return string Return path to file
     */
    public function getFilePath($field)
    {
        return rtrim($this->getFolderPath($field), '/') . '/' . $this->getOwner()->{$field};
    }

    /**
     * Get file type
     * @author Orlov Alexey <aaorlov88@gmail.com>
     *
     * @param string $field Field name
     * @param boolean $full Full or not full file type
     *
     * @return string Return file type
     */
    public function getFileType($field, $full = true)
    {
        $filePath = $this->getFilePath($field);
        if (empty($this->getOwner()->{$field}) || !file_exists($filePath)) {
            return '';
        }

        $mimeType = CFileHelper::getMimeType($filePath);
        if (null === $mimeType) {
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);
            $mimeType = CFileHelper::getMimeTypeByExtension($ext);
        }

        if (true === $full) {
            return $mimeType;
        }

        $mimeContentType = explode('/', $mimeType);

        return isset($mimeContentType[1]) ? $mimeContentType[1] : false;
    }

    /**
     * Get file src
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @retun string File src
     * @throws CException
     */
    public function getFileSrc($field, $default = null)
    {
        if (is_null($this->uploadUrl) && Yii::app() instanceof CWebApplication) {
            throw new CException('Public attribute "uploadUrl" is empty', 500);
        } elseif (is_null($this->uploadUrl) && Yii::app() instanceof \CConsoleApplication) {
            $url = '';
        } else {
            if (!$this->getOwner()->{$field}) {
                return $default === null ? '' : $default;
            }
            $url = rtrim($this->uploadUrl, '/') . rtrim($this->getRelativeFolderPath(), '/') . '/' . $field . '/' . $this->getOwner()->{$field};
        }
        return $url;
    }

    /**
     * Save attributes to temp data
     * @param CEvent $event event parameter
     */
    public function afterFind($event)
    {
        foreach ($this->fields as $field) {
            if ($this->owner->hasAttribute($field)) {
                $this->attributes[$field] = $this->owner->{$field};
            }
        }

        return parent::afterFind($event);
    }

    /**
     * Prepare fields before validate
     * @param CEvent $event event parameter
     * @author Orlov Alexey <aaorlov88@gmail.com>
     */
    public function beforeValidate($event)
    {
        $this->prepareField();
        return parent::beforeValidate($event);
    }

    /**
     * If model save with flag false, call method for prepare field
     * @param CEvent $event event parameter
     */
    public function beforeSave($event)
    {
        $this->prepareField();
        return parent::beforeSave($event);
    }

    /**
     * After save, file move in folder for model and delete temp file
     * @param CEvent $event event parameter
     * @author Orlov Alexey <aaorlov88@gmail.com>
     * @throws CException
     */
    public function afterSave($event)
    {
        if (Yii::app() instanceof CWebApplication) {
            foreach ($this->fields as $field) {
                if ($this->getFileByKey($field)) {
                    if (!is_dir($this->getFolderPath($field))) {
                        if (!@mkdir($this->getFolderPath($field), $this->mode, true)) {
                            $this->cleanOnException($field);
                            throw new CException('Can\'t create dir: ' . $this->getFilePath($field), 500);
                        }
                    }
                    $this->owner->{$field} = $this->fieldsValues[$field];
                    if (!@copy($this->getFileByKey($field), $this->getFilePath($field))) {
                        $this->cleanOnException($field);
                        throw new CException('File can\'t copy from ' . $this->getFileByKey($field) . ' to dest: ' . $this->getFilePath($field), 500);
                    }
                    $this->resetFile($field);
                }
                $this->resetFile($field);
            }
        }
        return parent::afterSave($event);
    }

    /**
     * Delete files from the server before removing data from the database.
     * @param CEvent $event event parameter
     * @author Orlov Alexey <aaorlov88@gmail.com>
     */
    public function afterDelete($event)
    {
        $this->removeFiles();

        return parent::afterDelete($event);
    }

    /**
     * Generate session key for file
     * @param string $field Field name
     * @return string
     */
    protected function generateFileKey($field)
    {
        return ('file_' . $this->owner->tableName() . $field);
    }

    /**
     * Clean data if was exeption or return old data if record is not new
     */
    protected function cleanOnException($field)
    {
        if (!$this->owner->isNewRecord) {
            $this->owner->attributes = $this->attributes;
            $this->owner->updateByPk($this->owner->{$this->primaryKey}, $this->attributes);
        }
        $this->resetFile($field);
    }

    /**
     * Prepare field for model
     * @author Orlov Alexey <aaorlov88@gmail.com>
     */
    protected function prepareField()
    {
        foreach ($this->fields as $field) {
            if (false === $this->isPrepared($field) && $this->owner->hasAttribute($field)) {
                if ($this->getFileByKey($field)) {
                    $explodeFilePath = explode('/', $this->getFileByKey($field));
                    $this->getOwner()->{$field} = end($explodeFilePath);
                } elseif (!file_exists($this->getFilePath($field))) {
                    $this->getOwner()->{$field} = null;
                }
            }
            //Set prepare true
            $this->fieldsValues[$field] = $this->getOwner()->{$field};
        }
    }

    /**
     * Check if field prepare
     * @param string $field Field name
     * @return bool
     */
    protected function isPrepared($field)
    {
        if (false === isset($this->fieldsValues[$field])) {
            return false;
        }
        return true === $this->fieldsValues[$field];
    }

}
