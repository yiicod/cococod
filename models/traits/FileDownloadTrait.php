<?php

namespace yiicod\cococod\models\traits;

use Yii;
use yiicod\cococod\libraries\UploadHandler;

/**
 * Coco trait uploader
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
trait FileDownloadTrait
{
    public static function download($fileName, $filePath, $clear = true)
    {
        $upload = new UploadHandler([
            'download_via_php' => true,
            // Read files in chunks to avoid memory limits when download_via_php
            // is enabled, set to 0 to disable chunked reading of files:
            'readfile_chunk_size' => 10 * 1024 * 1024, // 10 MiB
            // Defines which files can be displayed inline when downloaded:
            'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
            // Defines which files (based on their names) are accepted for upload:
            'accept_file_types' => '/.+$/i',
        ], false);
        $upload->downloadFile($fileName, $filePath, $clear);
    }
}