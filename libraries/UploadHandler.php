<?php

namespace yiicod\cococod\libraries;

use CFileHelper;
use UploadHandler as BaseUploadHandler;
use Yii;

Yii::import('vendor.blueimp.jQuery-File-Upload.server.php.UploadHandler');

class UploadHandler extends BaseUploadHandler
{
    public $userPostFunc = null;

    /**
     * Post trigger.
     *
     * @param bool $print_response
     *
     * @return string
     */
    public function post($print_response = true)
    {
        if (Yii::app()->request->getParam('_method') === 'DELETE') {
            return $this->delete($print_response);
        }
        $upload = isset($_FILES[$this->options['param_name']]) ?
            $_FILES[$this->options['param_name']] : null;
        // Parse the Content-Disposition header, if available:
        $file_name = $this->get_server_var('HTTP_CONTENT_DISPOSITION') ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/', '', $this->get_server_var('HTTP_CONTENT_DISPOSITION')
            )) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range = $this->get_server_var('HTTP_CONTENT_RANGE') ?
            preg_split('/[^0-9]+/', $this->get_server_var('HTTP_CONTENT_RANGE')) : null;
        $size = $content_range ? $content_range[3] : null;
        $files = [];
        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $this->file_type_executable($upload['tmp_name'][$index]);
                $file = $this->handle_file_upload(
                    $upload['tmp_name'][$index], $file_name ? $file_name : $upload['name'][$index], $size ? $size : $upload['size'][$index], $upload['type'][$index], $upload['error'][$index], $index, $content_range
                );
                //Callback
                $file = $this->postCallback($file);
                $files[] = $file;
            }
        } else {
            $this->file_type_executable($upload['tmp_name']);
            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:
            $file = $this->handle_file_upload(
                isset($upload['tmp_name']) ? $upload['tmp_name'] : null, $file_name ? $file_name : (isset($upload['name']) ?
                $upload['name'] : null), $size ? $size : (isset($upload['size']) ?
                $upload['size'] : $this->get_server_var('CONTENT_LENGTH')), isset($upload['type']) ?
                $upload['type'] : $this->get_server_var('CONTENT_TYPE'), isset($upload['error']) ? $upload['error'] : null, null, $content_range
            );

            //Callback
            $file = $this->postCallback($file);
            $files[] = $file;
        }

        return $this->generate_response(
            [$this->options['param_name'] => $files], $print_response
        );
    }

    public function downloadFile($fileName, $filePath)
    {
        // Prevent browsers from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if (!preg_match($this->options['inline_file_types'], $fileName)) {
            $this->header('Content-Type: application/octet-stream');
            $this->header('Content-Disposition: attachment; filename="' . $fileName . '"');
        } else {
            $this->header('Content-Type: ' . CFileHelper::getMimeType($filePath));
            $this->header('Content-Disposition: inline; filename="' . $fileName . '"');
        }
        $this->header('Content-Length: ' . $this->get_file_size($filePath));
        $this->header('Last-Modified: ' . gmdate('D, d M Y H:i:s T', filemtime($filePath)));

        $this->readfile($filePath);
    }

    /**
     * Run initialize method.
     */
    public function run()
    {
        $this->initialize();
    }

    /**
     * Callback on post end method.
     *
     * @param $file
     * @return mixed
     */
    public function postCallback($file)
    {
        //Callback
        $content_range = $this->get_server_var('HTTP_CONTENT_RANGE') ?
            preg_split('/[^0-9]+/', $this->get_server_var('HTTP_CONTENT_RANGE')) : null;

        if (($this->get_server_var('HTTP_CONTENT_RANGE') && $content_range[3] - 1 == $content_range[2]) ||
            !$this->get_server_var('HTTP_CONTENT_RANGE')
        ) {
            $file = call_user_func_array($this->userPostFunc, [$file, $this->get_upload_path()]);
        }

        return $file;
    }

    protected function file_type_executable($file)
    {
        if ('text/x-php' == mime_content_type($file)) {
            throw new \Exception('File type was blocked', 404);
        }
    }

    protected function trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        $name = preg_replace('/[^A-Za-z0-9\-\.]/', '_', $name);

        return parent::trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range);
    }

    protected function upcount_name($name)
    {
        return preg_replace_callback(
            '/(?:(?:_([\d]+))?(\.[^.]+))?$/', [$this, 'upcount_name_callback'], $name, 1
        );
    }

    protected function upcount_name_callback($matches)
    {
        $index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';

        return '_' . $index . $ext;
    }
}
