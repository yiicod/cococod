Coco Cod multi ajax uploader
============================

Insert into your composer:
--------------------------
```php
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "blueimp/jQuery-File-Upload",
            "version": "9.10.1",
            "source": {
                "type": "git",
                "url": "https://github.com/blueimp/jQuery-File-Upload",
                "reference": "9.10.1"
            }
        }
    },
    {
        "type": "git",
        "url": "https://bitbucket.org/codteam/yiicod-cococod.git"
    }
]
```

Using
-----

Widget for uploader button. This uploader based on the jQuery-File-Upload
--------------------------------------------------------------------------

Insert into config
------------------

```php
'aliases' => array(
    'vendor' => '...',
),
```

```php

Settings for widget: 'yiicod\cococod\widgets\FileUploadWidget'

$this->widget('yiicod\cococod\widgets\FileUploadWidget', array(
    'id' => 'cocowidget',
    'allowedExtensions' => array('jpeg', 'jpg', 'gif', 'png'),
    'maxFileSize' => 2 * 1024 * 1024, // limit in server-side and in client-side 2mb
    'uploadDir' => Yii::getPathOfAlias('webroot') . '/uploads/temp/', // coco will @mkdir it
    'uploadUrl' => Yii::app()->getBaseUrl(true) . '/uploads/temp/', // coco will @mkdir it
    'uploader' => 'UserAvatar',
    'userdata' => array('userId' => $userModel->id),    
    'maxUploads' => -1, // defaults to -1 (unlimited)   
    'multiple' => false, // true or false, defaults: true
    'buttonText' => 'Upload file',
    'dropFilesText' => 'Upload or Drop here',
    'clientOptions' => array(
        //For chunk uploded
        'maxChunkSize' => 10000000
    ),
    'events' => array(
        //If is not chunk then
        'done' => 'js: function(e, data){ $(".avatar").html("<img src=" + data.result.files[0].url + " />"); }'
        //If uses chunk then
        'chunkdone' => 'js: function(e, data){
            $.fn.yiiListView.update("rewardList"); $("#cocowidget-photo .files").html(" "); 
        }',
    ),
    'htmlOptions' => array('style' => 'width: 300px;'),
    'defaultUrl' => array('site/cocoCod', array()),    
));
```

Insert into controller
----------------------
```php
public function actions()
{
    return array(
        'cocoCod' => array(
            'class' => 'cococod\actions\FileUploadAction',
        ),
    );
}
```

Insert into model 
-----------------


```php
public function behaviors()
{
    return array(
        'FileUploadBehavior' => array(
            'class' => 'yiicod\cococod\models\behaviors\FileUploadBehavior',
            'uploadDir' => Yii::getPathOfAlias('webroot') . '/uploads/', //Dir for file save
            'uploadUrl' => Yii::app()->getBaseUrl(true) . '/uploads/', // Base url to folder
            'fields' => array('logo'),            
        ),
    );
}
```

Upload immediately
------------------

```php
class UserAvatar implement UploaderInterface{
    /**
     * Event for coco uploader
     * @param string $fullFileName Full file path
     * @param Array $userdata Userdata from widget
     * @param Array $results Uploaded result file
     * @return Array or null
     */
    public function uploading($fullFileName, $userdata, $results)
    {  
        $model = new UserModel();
        //Save to temp
        $model->onAfterFileUploaded($fullFileName, 'logo');
    
        //After save requered set
        if ($model->save()) {
                return array(
                    'url' => $model->getFileSrc('logo'),        
                    '...' => '...'
                );
            )else{
                //Delete temp uploaded file
                $model->resetFile('logo');
                return array(
                    'error' => 'Insert error message'
                    '...' => '...'
                );            
            };
        }
    }
}
```

Upload on submit
----------------

```php
class UserAvatar implement UploaderInterface{
    
    /**
     * Event for coco uploader
     * @param string $fullFileName Full file path
     * @param Array $userdata Userdata from widget
     * @param Array $results Uploaded result file
     * @return Array or null
     */
    public function uploading($fullFileName, $userdata, $results)
    { 
        $model = new UserModel();
        //Save to temp
        $model->onAfterFileUploaded($fullFileName, 'logo');
    }
}
```