<?php

namespace yiicod\cococod\helpers;

use CLogger;
use Yii;

/**
 * File upload helper.
 *
 * @author Orlov Alexey <aaorlov88@gmail.com>
 */
class FileUploadHelper
{
    /**
     * Recursive delete the directory and all its files.
     *
     * @param string $dir File or folder that should be deleted
     */
    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    if (filetype($dir . '/' . $object) == 'dir') {
                        self::rrmdir($dir . '/' . $object);
                    } else {
                        if (false === @unlink($dir . '/' . $object)) {
                            Yii::log(sprintf('Can not "unlink" %s', $dir . '/' . $object), CLogger::LEVEL_ERROR, 'system.cococod');
                        }
                    }
                }
            }
            reset($objects);
            if (false === @rmdir($dir)) {
                Yii::log(sprintf('Can not "rmdir" %s', $dir . '/' . $object), CLogger::LEVEL_ERROR, 'system.cococod');
            }
        }
    }
}
