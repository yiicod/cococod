<?php

namespace yiicod\cococod\widgets;

/*
 * CocoCodWidget
 * @author Orlov Alexey <Orlov.Alexey@zfort.net>
 * @version $Id$
 * @package extensions
 * @since 1.0
 */
use CActiveRecord;
use CClientScript;
use CException;
use CHtml;
use CHttpRequest;
use CJavaScript;
use CJavaScriptExpression;
use CLogger;
use CMap;
use CWidget;
use ErrorException;
use Exception;
use Yii;
use yiicod\cococod\libraries\UploadHandler;
use yiicod\cococod\models\base\UploaderInterface;

class FileUploadWidget extends CWidget
{
    /**
     * Widget id.
     *
     * @var string
     */
    public $id = 'cocowidget';

    /**
     * Additional html inside widget.
     *
     * @var string
     */
    public $html = '';

    /**
     * Html options.
     *
     * @var array
     */
    public $htmlOptions = [];

    /**
     * Defautl url.
     *
     * @var array
     */
    public $defaultUrl = ['site/cocoCod', []];

    /**
     * Upload dir path ( For temp ).
     *
     * @var string
     */
    public $uploadDir = 'assets';

    /**
     * File url ( For temp ).
     *
     * @var string
     */
    public $uploadUrl;

    /**
     * Button text.
     *
     * @var string
     */
    public $buttonText = 'Find & Upload';

    /**
     * Button text on drag&drop.
     *
     * @var string
     */
    public $dropFilesText = 'Drop Files Here';

    /**
     * Allowed extensions, this validate at first on client side,
     * then on server side.
     *
     * @var array
     */
    public $allowedExtensions = [];

    /**
     * Max file size, 10 MB.
     *
     * @var int
     */
    public $maxFileSize = 10000000;

    /**
     * Min file size.
     *
     * @var int
     */
    public $minFileSize;

    /**
     * Allow multi file selection.
     *
     * @var bool
     */
    public $multiple = true;
    /**
     * Max uploads for one time. -1 is unlimited.
     *
     * @var int
     */
    public $maxUploads = -1;

    /**
     * jQuery file uploader events.
     *
     * @var array
     */
    public $events = [];

    /**
     * CActiveModel.
     *
     * @var CActiveRecord $model
     */
    public $model = null;

    /**
     * Model attribute = null;.
     *
     * @var string
     */
    public $attribute = null;

    /**
     * Class name
     *
     * @var UploaderInterface string
     */
    public $uploader;

    /**
     * User data, send in receptorClass method.
     *
     * @var array
     */
    public $userdata;

    /**
     * UploadHandler options.
     *
     * @var array
     */
    public $serverOptions = [];

    /**
     * jQuery file upload options ( client side ).
     *
     * @var array
     */
    public $clientOptions = [];

    /**
     * Base view name.
     *
     * @var string
     */
    public $view = 'base';

    /**
     * Error message.
     *
     * @var array
     */
    public $errorMessages = [];

    /**
     * Availabeled events.
     *
     * @var array
     */
    protected $callbackOptions = [
        'add', 'submit', 'send', 'done', 'fail', 'always', 'progress',
        'progressall', 'start', 'stop', 'change', 'paste', 'drop', 'dragover',
        'chunksend', 'chunkdone', 'chunkfail', 'chunkalways', 'processstart',
        'process', 'processdone', 'processfail', 'processalways', 'processstop',
        //Additional Callback Options for the UI
        'destroy', 'destroyed', 'added', 'sent', 'completed', 'failed', 'finished',
        'started', 'stopped',
    ];
    /**
     * Assets options.
     *
     * @var array
     */
    protected $assetsOptions = [];

    public function runAction($serverOptions, $data)
    {
        header('Vary: Accept');
        (Yii::app()->request->getAcceptTypes() &&
            (strpos(Yii::app()->request->getAcceptTypes(), 'application/json') !== false)
        ) ? header('Content-type: application/json') : header('Content-type: text/plain');

        set_error_handler([$this, 'errorHandler']);

        try {
            $options = $this->decodeServerOptions($data);
            //Merge with action data
            $options['serverOptions'] = CMap::mergeArray($options['serverOptions'], $serverOptions);

            foreach ($options as $attr => $value) {
                if (property_exists($this, $attr)) {
                    $this->{$attr} = $value;
                }
            }

            $fileHandler = new UploadHandler($options['serverOptions'], false, $options['errorMessages']);
            $fileHandler->userPostFunc = [$this, 'onFileUpload'];
            $fileHandler->run();
        } catch (Exception $e) {
            header(sprintf('%s 500 %s', $_SERVER['SERVER_PROTOCOL'], Yii::t('cococod', 'File has broken')), true, 500);
            Yii::log('FileUploadWidget: ' . $e->getMessage() . ':' . $e->getFile() . ':' . $e->getLine(), CLogger::LEVEL_ERROR, 'system.cococod');
            Yii::app()->end();
        }
    }

    /**
     * Decode server options.
     *
     * @param array $data
     *
     * @return array $data Return array of data
     *
     * @throws Exception
     */
    protected function decodeServerOptions($data)
    {
        if (null === Yii::app()->cache) {
            throw new CException('Can not find "cache" component', 500);
        }
        $vars = Yii::app()->cache->get($data);

        if (false === $vars && isset($_FILES['files'])) {
            throw new CException(('Incorrect data: ' . $data . ' hash: ' . md5($data)), 406);
        }

        $vars = @unserialize(base64_decode($vars));
        if (false === $vars) {
            throw new CException('Incorrect data decode', 400);
        }

        return $vars;
    }

    /**
     * @todo Need refactoring
     *
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     *
     * @throws ErrorException
     */
    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        if (in_array($errno, [E_ERROR])) {
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
        }
    }

    /**
     * Callback for upload file.
     *
     * @param array $file
     * @param string $path
     *
     * @return array
     */
    public function onFileUpload($file, $path)
    {
        $file = array_diff_key((array)$file, array_flip([
            'deleteUrl', 'deleteType', 'url',
        ]));
        $fullPath = (rtrim($path, '/') . '/' . $file['name']);

        if (false === isset($file['error'])) {
            $file['status'] = 'success';
            $onFileUploadedResult = $this->onFileUploaded($fullPath, $this->userdata, $file);
            if (is_array($onFileUploadedResult)) {
                $file = CMap::mergeArray($file, $onFileUploadedResult);
            }
        } else {
            $file['status'] = 'error';
            $file = array_diff_key($file, array_flip([
                'url',
            ]));
        }

        return $file;
    }

    /**
     * Event on file uploader, call model metod.
     *
     *
     * @param string $filePath Full file path
     * @param array $userdata User data form widget
     * @param array $result Return result to widget
     * @return mixed
     * @throws Exception
     */
    private function onFileUploaded($filePath, $userdata, $result)
    {
        $uploader = $this->uploader;
        /** @var UploaderInterface $inst */
        $inst = new $uploader();

        if (false === $inst instanceof UploaderInterface) {
            throw new CException(sprintf('Uploader class must be instanceof UploaderInterface'));
        }
        return $inst->uploading($filePath, $userdata, $result);
    }

    /**
     * Run widget.
     */
    public function run()
    {
        $this->registerCoreScripts();

        $this->render($this->view, [
            'id' => $this->id,
            'html' => $this->html,
            'multiple' => $this->multiple,
            'clientOptions' => $this->generateClientOptions(),
            'events' => $this->generateEvents(),
            'buttonText' => addslashes($this->buttonText),
            'dropFilesText' => addslashes($this->dropFilesText),
            'hiddenField' => $this->genereteActiveHiddenField(),
            'htmlOptions' => $this->generateHtmlOptions(),
        ]);
    }

    /**
     * Register core script for uploader.
     */
    public function registerCoreScripts()
    {
        $this->assetsOptions['jsVendorUrl'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.blueimp.jQuery-File-Upload.js'));
        $this->assetsOptions['imgVendorUrl'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.blueimp.jQuery-File-Upload.img'));

        $this->assetsOptions['cssVendorUrl'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.blueimp.jQuery-File-Upload.css'));
        //$this->assetsOptions['corsVendorUrl'] = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.blueimp.jQuery-File-Upload.cors'));

        $this->assetsOptions['assets'] = Yii::app()->getAssetManager()->publish(realpath(dirname(__FILE__) . '/../assets'));

        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');

        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.iframe-transport.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-process.js', CClientScript::POS_END);
//        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/vendor/jquery.ui.widget.js', CClientScript::POS_END);
//        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-image.js', CClientScript::POS_END);
//        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-audio.js', CClientScript::POS_END);
//        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-video.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-validate.js', CClientScript::POS_END);
//        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['jsVendorUrl'] . '/jquery.fileupload-ui.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($this->assetsOptions['assets'] . '/jquery.fileupload-trigger.js', CClientScript::POS_END);

        Yii::app()->clientScript->registerCssFile($this->assetsOptions['assets'] . '/jquery.fileupload-trigger.css');
        Yii::app()->clientScript->registerCssFile($this->assetsOptions['cssVendorUrl'] . '/jquery.fileupload.css');
        Yii::app()->clientScript->registerCssFile($this->assetsOptions['cssVendorUrl'] . '/jquery.fileupload-ui.css');
    }

    /**
     * Generate client options ( uploader js config ).
     *
     * @return string the encoded string
     */
    protected function generateClientOptions()
    {
        $this->clientOptions = CMap::mergeArray([
            'dataType' => 'json',
            'acceptFileTypes' => 'js: /(\.|\/)(' . implode('|', $this->allowedExtensions) . ')$/i',
            'maxFileSize' => $this->maxFileSize,
            'minFileSize' => $this->minFileSize,
            'maxNumberOfFiles' => $this->maxUploads == -1 ? '' : $this->maxUploads,
            'dropZone' => '#' . $this->id . ' .dropzone',
            'messages' => [
                'maxNumberOfFiles' => Yii::t('cococod', 'Maximum number of files exceeded'),
                'acceptFileTypes' => Yii::t('cococod', 'File type not allowed'),
                'maxFileSize' => Yii::t('cococod', 'File is too large'),
                'minFileSize' => Yii::t('cococod', 'File is too small'),
            ],
        ], $this->clientOptions);

        $this->clientOptions['url'] = $this->generateUploadUrl();

        return CJavaScript::encode($this->clientOptions);
    }

    /**
     * Generate upload url.
     *
     * @return string
     */
    protected function generateUploadUrl()
    {
        $route = $this->defaultUrl[0];
        $url = '';
        $params = [];
        $params['action'] = 'upload';
        $params['data'] = $this->encodeServerOptions();
        $params = CMap::mergeArray(array_slice($this->defaultUrl, 1), $params);

        if (Yii::app()->urlManager->urlFormat == 'path') {
            $ampersand = '&';
            $url = Yii::app()->createAbsoluteUrl($route);
            if (!Yii::app()->urlManager->showScriptName) {
                $url .= '/';
            }
            if (($query = Yii::app()->urlManager->createPathInfo($params, '=', $ampersand)) !== '') {
                $url .= '?' . $query;
            }
        }

        return $url;
    }

    /**
     * Encode server options.
     * decode data.
     *
     * @return string $data Return encode array of data
     *
     * @throws Exception
     */
    protected function encodeServerOptions()
    {
        $vars = [
            'serverOptions' => [
                'max_file_size' => $this->maxFileSize, // server-side size validation
                'min_file_size' => $this->minFileSize, // server-side size validation
                'multiple' => true, // true or false. allow the user to select multiple files at once.
                'max_number_of_files' => $this->maxUploads == -1 ? null : $this->maxUploads,
                'accept_file_types' => '/\.(' . implode('|', $this->allowedExtensions) . ')$/i',
                'upload_dir' => rtrim($this->uploadDir, '/') . '/',
                'upload_url' => rtrim($this->uploadUrl, '/') . '/',
            ],
            'errorMessages' => $this->errorMessages,
            'uploader' => $this->uploader,
            'userdata' => $this->userdata,
            'unique' => uniqid(),
            'id' => $this->id,
        ];

        $vars['serverOptions'] = CMap::mergeArray($vars['serverOptions'], $this->serverOptions);

        $data = base64_encode(serialize($vars));

        if (null === Yii::app()->cache) {
            throw new CException('Can not find "cache" component', 500);
        }

        Yii::app()->cache->set(md5($data), $data, 86400);

        return md5($data);
    }

    /**
     * Generate events ( uploader js events ).
     *
     * @return array
     * @throws CException
     */
    public function generateEvents()
    {
        $events = [];
        $callbackOptions = array_keys($this->events);
        $supportedOptions = [];
        foreach ($this->callbackOptions as $event) {
            if (in_array($event, $callbackOptions) ||
                in_array('fileupload' . $event, $callbackOptions)
            ) {
                $events['fileupload' . $event] = isset($this->events['fileupload' . $event]) ?
                    new CJavaScriptExpression($this->events['fileupload' . $event]) : new CJavaScriptExpression($this->events[$event]);
                $supportedOptions[] = (in_array($event, $callbackOptions) ? $event : ('fileupload' . $event));
            }
        }
        $diff = array_diff($callbackOptions, $supportedOptions);
        if (count($diff)) {
            throw new CException('Can not find this events: ' . implode(', ', $diff));
        }

        return CJavaScript::encode($events);
    }

    /**
     * Generate hidden field for CActiveForm.
     *
     * @return string
     */
    public function genereteActiveHiddenField()
    {
        $hiddenField = '';

        if (is_null($this->model) === false && $this->multiple === false) {
            $hiddenField = CHtml::activeHiddenField($this->model, $this->attribute);
        }

        return $hiddenField;
    }

    /**
     * Generate html options for widget.
     *
     * @return array
     */
    public function generateHtmlOptions()
    {
        $this->htmlOptions['id'] = $this->id;
        $this->htmlOptions['class'] = isset($this->htmlOptions['class']) ?
            'cococod ' . $this->htmlOptions['class'] : 'cococod';
        $this->htmlOptions['inited'] = 0;

        return $this->htmlOptions;
    }
}
