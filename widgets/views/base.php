<?php echo CHtml::openTag('span', $htmlOptions); ?>
<?php echo $html ?>
    <!-- The fileinput-button span is used to style the file input field as button -->

    <span class="fileinput-button dropzone">
        <span class="button-wrap"><?php echo $buttonText ?></span>
        <!-- The file input field used as target for the file upload widget -->
        <input class="fileupload" type="file" name="files[]" <?php echo $multiple ? 'multiple' : '' ?>>
    </span>
    <!-- The global progress bar -->
    <div class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div class="files"></div>
<?php echo $hiddenField; ?>
<?php echo CHtml::closeTag('span'); ?>

<?php

Yii::app()->clientScript->registerScript('file-uploader-widget-'.$id, '$("#'.$id.'").fileuploadtrigger({
        id: "'.$id.'",
        settings: '.$clientOptions.',
        events: '.$events.',
        messages: {
            doneText: "'.Yii::t('cococod', 'Done.').'",
            errorText: "'.Yii::t('cococod', 'Error.').'",
            dropText: "'.$dropFilesText.'",
            selectText: "'.$buttonText.'"
        },
})');

?>