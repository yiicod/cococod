(function ($) {
    var FileUploadTrigger = function (element, options) {
        this.options = $.extend({}, this.DEFAULTS, options);
        this.element = $(element);

        this.init();
    };

    FileUploadTrigger.VERSION = '1.0.0';

    FileUploadTrigger.DEFAULTS = {
        settings: {},
        events: {},
        messages: {
            doneText: 'Are you sure?',
            errorText: '',
            dropText: '',
            selectText: ''
        }
    };

    FileUploadTrigger.PUBLIC = [];

    FileUploadTrigger.prototype.init = function (element, options) {
        this.element.find('.fileupload').fileupload(this.options.settings);

        this.defaultEvents();
        this.customEvents();
        this.DragAndDrop();

        this.element.attr('inited', 1);

        setInterval($.proxy(function () {
            if (!$('#' + this.options.id).attr('inited')) {
                this.element = $('#' + this.options.id);

                this.defaultEvents();
                this.customEvents();
                this.DragAndDrop();

                this.element.attr('inited', 1);
            }
        }, this), 200);
    };

    FileUploadTrigger.prototype.defaultEvents = function () {
        this.element.find('.fileupload')
            .on('fileuploadprocessalways', $.proxy(function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = this.element.find('.files'),
                    input = this.element.find('input[type="hidden"]');

                if (file.error) {
                    input.empty();
                    node.append('<p class="danger">' + file.name + ' - ' + file.error + '</p>');
                }
            }, this))
            .on('fileuploadprogressall', $.proxy(function (e, data) {
                this.element.find('.progress').show('fast');
                var progress = parseInt(data.loaded / data.total * 100, 10);
                this.element.find('.progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }, this))
            .on('fileuploaddone', $.proxy(function (e, data) {
                $.each(data.result.files, $.proxy(function (index, file) {
                    var node = this.element.find('.files'),
                        input = this.element.find('input[type="hidden"]');
                    if (file.error) {
                        input.empty();
                        node.append('<p class="danger">' + file.name + ' - ' + file.error + '</p>');
                    } else {
                        input.val(file.name);
                        node.append('<p class="success">' + file.name + ' - ' + this.options.messages.doneText + '</p>');
                    }
                }, this));
            }, this))
            .on('fileuploadfail', $.proxy(function (e, data) {
                var node = this.element.find('.files'),
                    input = this.element.find('input[type="hidden"]');
                $.each(data.files, $.proxy(function (index, file) {
                    input.empty();
                    node.append('<p class="text-danger">' + file.name + ' ' + data.errorThrown + '</p>');
                }, this));
            }, this))
            .prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    };

    FileUploadTrigger.prototype.customEvents = function () {
        for (var key in this.options.events) {
            this.element.find('.fileupload').on(key, this.options.events[key]);
        }
    };

    FileUploadTrigger.prototype.DragAndDrop = function () {
        $(document)
            .on('dragover', $.proxy(function (e) {
                var dropZone = this.element.find('.dropzone'),
                    foundDropzone,
                    timeout = window.dropZoneTimeout;
                if (!timeout) {
                    dropZone.addClass('in');
                    dropZone.children('.button-wrap').text(this.options.messages.dropText);
                } else {
                    clearTimeout(timeout);
                }
                var found = false,
                    node = e.target;
                do {
                    if ($(node).hasClass('dropzone')) {
                        found = true;
                        foundDropzone = $(node);
                        break;
                    }
                    node = node.parentNode;
                } while (node != null);
                dropZone.removeClass('in hover');
                dropZone.children('.button-wrap').text(this.options.messages.selectText);
                if (found) {
                    foundDropzone.addClass('hover');
                    foundDropzone.children('.button-wrap').text(this.options.messages.dropText);
                }
            }, this))
            .on('drop', '#' + this.element.attr('id') + '.dropzone', $.proxy(function (e, data) {
                var dropZone = this.element.find('.dropzone');
                dropZone.removeClass('in hover');
                dropZone.children('.button-wrap').text(this.options.messages.selectText);
            }, this));
    };

    FileUploadTrigger.prototype.listener = function () {

    };

    // Enable iframe cross-domain access via redirect option:
    // @todo Need implemente normal callback
    //$('#<?= $id ?> .fileupload').fileupload(
    //        'option',
    //        'redirect',
    //                '<?= rtrim(Yii::app()->getBaseUrl(true), '/') ?><?= $assetsOptions['corsVendorUrl'] ?>/result.html'
    //        );

    // FileUploadTrigger PLUGIN DEFINITION
    // ==================================
    function Plugin(option) {
        return this.each(function () {
            var $this = $(this);
            var data = $this.data('fileuploadtrigger');

            if (!data) {
                $this.data('fileuploadtrigger', (data = new FileUploadTrigger(this, option)));
            } else if (typeof option == 'string' && $.inArray(arguments[0], FileUploadTrigger.PUBLIC) !== -1) {
                data[option].apply(data, Array.prototype.slice.call(arguments, 1));
            } else {
                throw new Error('Can not run class');
            }
        });
    }

    $.fn.fileuploadtrigger = Plugin;
    $.fn.fileuploadtrigger.Constructor = FileUploadTrigger;
}(jQuery));